aws eks --region us-east-1 update-kubeconfig --name eks4api-demo
kubectl apply -f aws-auth.yaml
kubectl apply -f pod.yaml
kubectl apply -f service.yaml
echo -e "\n\n::::::: Pods ::::::::::::::::\n"
kubectl get pods
echo -e "\n\n::::::: Pod: api-test ::::::::::::::::\n"
kubectl describe pod api-demo
echo -e "\n\n::::::: Service: api-test-svc ::::::::::::::::\n"
kubectl describe service api-demo-svc
echo -e "\n\n::::::: Services ::::::::::::::::\n"
kubectl get services
